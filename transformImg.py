#!/usr/bin/python

import sys
import getopt
import os
import shlex
import subprocess
import time


def execute_command(command, verbose):
    if verbose:
        print ">> " + command

    if os.name == 'nt':
        os.system(command)
        time.sleep(0.5)
    else:
        args = shlex.split(command)
        subprocess.Popen(args).communicate()


def get_os_param(param):

    if param == "slash":
        if os.name == 'nt':
            return "\\"
        else:
            return "/"

    if param == "cmd_escape":
        if os.name == 'nt':
            return "^>"
        else:
            return "\\>"

    if param == "space_replace":
        if os.name == 'nt':
            return " "
        else:
            return "\ "

    if param == "quote":
        if os.name == 'nt':
            return "\""
        else:
            return ""


def print_params(height, width, dpi, threshold, folder, pattern):
    print "==== Params ===="
    print "max heigth = " + height + "px"
    print "max width = " + width + "px"
    print "dpi = " + dpi
    print "threshold = " + threshold + "%"
    print "folder = " + folder
    print "pattern = " + pattern
    print "================"


def run(argv):
    opts, argv = getopt.getopt(argv, "y:h:w:f:t:d:vp",
                               ["--pattern", "--height", "--width", "--folder", "--threshold", "--dpi", "--verbose", "--params"])
    threshold = "70"
    dpi = "300"
    width = "500"
    height = "500"
    folder = "."
    verbose = None
    print_p = None
    pattern = "*#*#_"
    acceptable_formats = ["jpg", "JPG", "png", "PNG", "bmp", "BMP", "gif", "GIF"]


    # Parse arguments
    for opt, arg in opts:
        # Get height
        if opt in ('-h', '--height'):
            height = arg

        # Get width
        if opt in ('-w', '--width'):
            width = arg

        # Get folder
        if opt in ('-f', '--folder'):
            folder = arg

        # Get threshold
        if opt in ('-t', '--threshold'):
            threshold = arg

        # Get dpi
        if opt in ('-d', '--dpi'):
            dpi = arg

        # Verbose
        if opt in ('-v', '--verbose'):
            verbose = True

        # Print params
        if opt in ('-p', '--params'):
            print_p = True

        # Pattern
        if opt in ('-y', '--pattern'):
            pattern = arg

    if print_p:
        print_params(height, width, dpi, threshold, folder, pattern)

    slash = get_os_param("slash")
    cmd_escape = get_os_param("cmd_escape")
    space_replace = get_os_param("space_replace")
    naming_space = space_replace
    quote = get_os_param("quote")
    name_right = ""
    name_left = ""

    pattern = pattern.replace("'", "")
    pattern = pattern.split("#")
    if len(pattern) > 2:
        naming_space = pattern[2]

    if len(pattern) > 1:
        if pattern[0] == "*":
            name_left = ""
        else:
            name_left = pattern[0]

        if pattern[1] == "*":
            name_right = ""
        else:
            name_right = pattern[1]

    if folder[-1] == slash:
        folder = folder[:-1]

    resized_directory = folder + slash + "images_resized" + slash
    bw_directory = folder + slash + "images_bw" + slash

    if not os.path.exists(resized_directory):
        os.makedirs(resized_directory)

    if not os.path.exists(bw_directory):
        os.makedirs(bw_directory)

    param_density = "-set units PixelsPerInch -density " + dpi
    param_alpha_remove = "-bordercolor white -border 0"
    param_threshold = "-threshold " + threshold + "%%"
    param_resize = "-resize " + width + "x" + height + cmd_escape
    param_colors = "-colors 2"

    for in_file in os.listdir(folder):
        name = in_file.split(".")
        if len(name) > 1:

            if name[1] in acceptable_formats:
                command = "convert %s %s %s %s %s" % (
                    quote + folder + slash + in_file.replace(" ", space_replace) + quote,
                    param_density,
                    param_alpha_remove,
                    param_resize,
                    quote + resized_directory + name_left + name[0].replace(" ", naming_space) + name_right + ".jpg" + quote)

                execute_command(command, verbose)

    for in_file in os.listdir(resized_directory):
        name = in_file.split(".")
        if len(name) > 1:

            if name[1] in acceptable_formats:
                command = "identify %s" % (quote + resized_directory + in_file + quote)
                execute_command(command, verbose)

                command = "convert %s %s %s %s %s" % (
                    quote + resized_directory + in_file + quote,
                    param_threshold, param_colors,
                    param_density,
                    quote + bw_directory + name[0] + naming_space + "BW.jpg" + quote)

                execute_command(command, verbose)

                command = "identify %s" % (quote + bw_directory + name[0] + naming_space + "BW.jpg" + quote)
                execute_command(command, verbose)


if __name__ == "__main__":
    run(sys.argv[1:])

